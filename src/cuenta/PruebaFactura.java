package cuenta;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class PruebaFactura {
    public static void main(String[] args)
    {
        Factura Ferreteria = new Factura("123456789","Clavos",50,2.50);
        System.out.printf("Producto #%s%nDescripcion: %s%nCantidad: %d%nPrecio Unitario: %.2f%nTotal a pagar: %.2f%n",
                Ferreteria.obtenerNumero(), Ferreteria.obteneDescripcion(), Ferreteria.obtenerCantidad(),
                Ferreteria.obtenerPrecioU(), Ferreteria.obtenerMontoFactura());
    }
    
}
