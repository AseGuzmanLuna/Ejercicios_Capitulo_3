package cuenta;
import java.util.Scanner;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class PruebaPerfilMedico {
     public static void main(String[] args)
    {
        Scanner entrada = new Scanner(System.in);
            System.out.print("¿Cuál es nombre(s)? ");
            String nom = entrada.nextLine();
            System.out.print("¿Cuales son tus apellidos? ");
            String app = entrada.nextLine();
            System.out.print("¿Cuál es tu sexo? ");
            String sexo = entrada.nextLine();
            System.out.println("¿Cuál es tu fecha de nacimiento? ");
            System.out.print("Día: ");
            int d = entrada.nextInt();
            System.out.print("Mes: ");
            int m = entrada.nextInt();
            System.out.print("Año: ");
            int a = entrada.nextInt();
            System.out.print("¿Cuál es tu altura en centímetros? ");
            double h = entrada.nextDouble();
            System.out.print("¿Cuál es tu peso en kilogramos? ");
            double p = entrada.nextDouble();
        PerfilMedico Ob2 = new PerfilMedico(nom,app,sexo,d,m,a,h,p);
        System.out.println("¿Qué día es hoy?");
            System.out.print("Día: ");
            d = entrada.nextInt();
            System.out.print("Mes: ");
            m = entrada.nextInt();
            System.out.print("Año: ");
            a = entrada.nextInt();
        Fecha hoy = new Fecha(d,m,a);
        int edad = Ob2.calculaEdad(a, m);
        int fcm = Ob2.frecuenciaCardiacaMaxima(edad);
        double fce = Ob2.frecuenciaCardiacaEsperada(fcm);
        double imc = Ob2.indiceMasaCorporal();
        Ob2.mostrarPerfilMédico(hoy, edad, fcm, fce, imc);
            
    }
}
