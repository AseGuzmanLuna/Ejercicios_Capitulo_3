package cuenta;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Fecha {
    private int dia;
    private int mes;
    private int año;
    
   //constructo que recibe 3 parámetros
    Fecha(int d, int m, int a){
        this.dia = d;
        this.mes = m;
        this.año = a;
    }

    public int obtenerDia() {
        return dia;
    }

    public void establecerDia(int dia) {
        this.dia = dia;
    }

    public int obtenerMes() {
        return mes;
    }

    public void establecerMes(int mes) {
        this.mes = mes;
    }

    public int obtenerAño() {
        return año;
    }

    public void establecerAño(int año) {
        this.año = año;
    }
    
    public void mostrarFecha(){
        if(obtenerMes()>0 && obtenerMes()<10)
            System.out.printf("%d / 0%d / %d%n", obtenerDia(), obtenerMes(), obtenerAño());
        if(obtenerMes()>9)
            System.out.printf("%d / %d / %d%n", obtenerDia(), obtenerMes(), obtenerAño());
    }
    
}
