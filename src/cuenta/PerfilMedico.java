package cuenta;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class PerfilMedico {
    private String nombre;
    private String apellido;
    private String sexo;
    private int dia;
    private int mes;
    private int año;
    private double altura;
    private double peso;
    //constructor que recibe 8 parámettros
    PerfilMedico(String n,String a,String s,int d,int m,int y,double h,double p){
        this.nombre = n;
        this.apellido = a;
        this.sexo = s;
        this.dia = d;
        this.mes = m;
        this.año = y;
        this.altura = h;
        this.peso = p;
    }
    //métodos obtener y establecer para cada atributo
        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getApellido() {
            return apellido;
        }

        public void setApellido(String apellido) {
            this.apellido = apellido;
        }

        public String getSexo() {
            return sexo;
        }

        public void setSexo(String sexo) {
            this.sexo = sexo;
        }

        public int getDia() {
            return dia;
        }

        public void setDia(int dia) {
            this.dia = dia;
        }

        public int getMes() {
            return mes;
        }

        public void setMes(int mes) {
            this.mes = mes;
        }

        public int getAño() {
            return año;
        }

        public void setAño(int año) {
            this.año = año;
        }

        public double getAltura() {
            return altura;
        }

        public void setAltura(double altura) {
            this.altura = altura;
        }

        public double getPeso() {
            return peso;
        }

        public void setPeso(double peso) {
            this.peso = peso;
        }
    //calcula la edad en años
        public int calculaEdad(int año, int mes)
        {
            int j=-1;
            for(int i=getAño() ; i<año ; i++)
            {
                j++;
            }
            if(mes > getMes())
                j++;
            return j;
        }
    //calcula la frecuencia cardiaca máxica
        public int frecuenciaCardiacaMaxima(int edad)
        {
            return 220-edad;
        }
    //método que calcula la frecuencia cardiaca esperada
    // en el 67.5%
        public double frecuenciaCardiacaEsperada(int fcm)
        {
            return fcm*0.675;
        }
    //método que caclula el indice de masa corporal    
        public double indiceMasaCorporal()
        {
            double altura1 = altura/100;
            return peso/(altura1*altura1);
            
        }
    public void mostrarPerfilMédico(Fecha f, int edad, int fcm, double fce, double imc){
        System.out.print("La fecha de hoy: ");
        f.mostrarFecha();
        System.out.printf("Nombre: %s %s %nSexo: %s %n",nombre, apellido, sexo);
            if(mes>0 && mes<10)
                System.out.printf("Fecha de nacimiento: %d / 0%d / %d%n", dia, mes, año);
            if(mes>9)
                System.out.printf("%d / %d / %d%n", dia, mes, año);
            System.out.printf("Altura: %.2f cm%n",altura);
            System.out.printf("Peso: %.2f kg%n",peso);
            System.out.printf("Edad: %d%n",edad);
            System.out.printf("Tu frecuencia cardiaca máxima es: %d%n",fcm);
            System.out.printf("Tu frecuencia cardiaca esperada es: %.2f%n",fce);
            System.out.printf("Tu índice de masa corporal es: %.2f%n",imc);
            System.out.println("Analiza en qué rango te encuentras:\nBajo peso: menos de 18.5\nNormal: entre 18.5 y 24.9\nSobrepeso: entre 25 y 29.9\nObeso: 30 o más");
    }
}
