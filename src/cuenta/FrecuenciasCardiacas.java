package cuenta;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class FrecuenciasCardiacas {
    private String nombre;
    private String apellido;
    private int dia;
    private int mes;
    private int año;
        //constructor que recibe 3 parámetros
        FrecuenciasCardiacas(String nom, String ap, int d, int m, int a)
        {
            this.nombre = nom;
            this.apellido = ap;
            this.dia = d;
            this.mes = m;
            this.año = a;
        }
        // método que obtiene el nombre
        public String obtenerNombre()
        {
            return nombre;
        }
        // método que establece el nombre
        public void establecerNombre(String n)
        {
            this.nombre = n;
        }
        // método que obtiene el apellido
        public String obtenerApellido()
        {
            return apellido;
        }
        // método que establece el apellido
        public void establecerApellido(String a)
        {
            this.apellido = a;
        }
      //método que obtiene el día  
        public int obtenerDia() {
        return dia;
        }
        //métodod que establece el día
        public void establecerDia(int dia) {
            this.dia = dia;
        }
        //método que obtiene el mes
        public int obtenerMes() {
            return mes;
        }
        //método que establece el mes
        public void establecerMes(int mes) {
            this.mes = mes;
        }
        //método que obtiene el año
        public int obtenerAño() {
            return año;
        }
        //metodo que establece el año
        public void establecerAño(int año) {
            this.año = año;
        }
        //método que calcula la edad
        public int calculaEdad(int año, int mes)
        {
            int j=-1;
            for(int i=obtenerAño() ; i<año ; i++)
            {
                j++;
            }
            if(mes > obtenerMes())
                j++;
            return j;
        }
        //método que calcula la frecuencia cardiaca máxima
        public int frecuenciaCardiacaMaxima(int edad)
        {
            return 220-edad;
        }
        //método que calcula la frecuencia cardiaca esperada
        // en el 67.5%
        public double frecuenciaCardiacaEsperada(int fcm)
        {
            return fcm*0.675;
        }
        public void mostrarDatosFCE(int fcm, double fce, int edad)
        {
            System.out.printf("%s %s%n",nombre, apellido);
            if(mes>0 && mes<10)
                System.out.printf("Fecha de nacimiento: %d / 0%d / %d%n", dia, mes, año);
            if(mes>9)
                System.out.printf("%d / %d / %d%n", dia, mes, año);
            System.out.printf("Edad: %d%n",edad);
            System.out.printf("Tu frecuencia cardiaca máxima es: %d%n",fcm);
            System.out.printf("Tu frecuencia cardiaca esperada es: %.2f%n",fce);
        }
    
}
