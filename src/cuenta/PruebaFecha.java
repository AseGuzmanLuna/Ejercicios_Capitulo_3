package cuenta;

import java.util.Scanner;

/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class PruebaFecha {
    public static void main(String[] args)
    {
        Scanner entrada = new Scanner(System.in);
        Fecha miCumple = new Fecha(11,04,1998);
        System.out.println("Mi cumpleaños:");
        miCumple.mostrarFecha();
        int d,m,a;
        System.out.println("¿Qué día es hoy?");
        System.out.print("Día: ");
        d = entrada.nextInt();
        System.out.print("Mes: ");
        m = entrada.nextInt();
        System.out.print("Año: ");
        a = entrada.nextInt();
        Fecha hoy = new Fecha(d,m,a);
        hoy.mostrarFecha();
    }
}
