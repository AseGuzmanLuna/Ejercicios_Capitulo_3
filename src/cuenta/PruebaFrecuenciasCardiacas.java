package cuenta;
import java.util.Scanner;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class PruebaFrecuenciasCardiacas {
     public static void main(String[] args)
    {
        Scanner entrada = new Scanner(System.in);
            System.out.println("¿Cuál es tu primer nombre?");
            String nom = entrada.nextLine();
            System.out.println("¿Cuál es tu primer apellido?");
            String app = entrada.nextLine();
            System.out.println("¿Cuál es tu fecha de nacimiento?");
            System.out.print("Día: ");
            int d = entrada.nextInt();
            System.out.print("Mes: ");
            int m = entrada.nextInt();
            System.out.print("Año: ");
            int a = entrada.nextInt();
        FrecuenciasCardiacas Ob1 = new FrecuenciasCardiacas(nom,app,d,m,a);
            System.out.println("¿Qué día es hoy?");
            System.out.print("Día: ");
            d = entrada.nextInt();
            System.out.print("Mes: ");
            m = entrada.nextInt();
            System.out.print("Año: ");
            a = entrada.nextInt();
        Fecha hoy = new Fecha(d,m,a);
        hoy.mostrarFecha();
        int edad = Ob1.calculaEdad(a,m);
        int fcm = Ob1.frecuenciaCardiacaMaxima(edad);
        double fce = Ob1.frecuenciaCardiacaEsperada(fcm);
        Ob1.mostrarDatosFCE(fcm,fce,edad);
    }
}
