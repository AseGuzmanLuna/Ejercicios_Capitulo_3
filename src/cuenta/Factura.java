package cuenta;
/**
 *
 * @author ASERET GUZMNÁN LUNA
 */
public class Factura {
    private String numero; // variable de instancia
    private String descripcion; // variable de instancia
    private int cantidad; // variable de instancia
    private double precioU; // variable de instancia
    
    // Constructor de factura que recibe 4 parámetros
    Factura(String num, String des, int cant, double pU){
        this.numero = num;
        this.descripcion = des;
        if(cant>0)
            this.cantidad = cant;
        else
            this.cantidad = 0;
        if(pU>0)
            this.precioU = pU;
        else 
            this.precioU = 0.00;
    }
        // método que obtiene el numero
        public String obtenerNumero()
        {
            return numero;
        }
        // método que establece el numero
        public void establecerNumero(String num)
        {
            this.numero = num;
        }
        // método que obtiene la descripcion
        public String obteneDescripcion()
        {
            return descripcion;
        }
        // método que establece la descripcion
        public void establecerDescripcion(String des)
        {
            this.descripcion = des;
        }
        //método que obtiene la cantidad
        public int obtenerCantidad()
        {
            return cantidad;
        }
        // método que establece la cantidad
        public void establecerCantidad(int cant)
        {
            if(cant>0)
                this.cantidad = cant;
            else
                this.cantidad = 0;
        }
        //método que obtiene el precio unitario
        public double obtenerPrecioU()
        {
            return precioU;
        }
        // método que establece el precio unitario
        public void establecerPrecioU(double pU)
        {
            if(pU>0)
                this.precioU = pU;
            else 
                this.precioU = 0.00;
        }
        //método que obtiene el total de la factura
        public double obtenerMontoFactura(){
            double mF = obtenerPrecioU()*obtenerCantidad();
            return mF;
        }
    
}
