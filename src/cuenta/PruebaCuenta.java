package cuenta;
/**
 *
 * @author ASERET GUZMÁN LUNA
 */
import java.util.Scanner;
public class PruebaCuenta {
    public static void main(String[] args)
        {
                Cuenta cuenta1 = new Cuenta("Jane Green", 50.00);
                Cuenta cuenta2 = new Cuenta("John Blue", -7.53);
                // muestra el saldo inicial de cada objeto
                cuenta1.mostrarCuenta();
                cuenta2.mostrarCuenta();
                // crea un objeto Scanner para obtener la entrada de la ventana de comandos
                Scanner entrada = new Scanner(System.in);
                System.out.print("Escriba el monto a depositar para cuenta1: "); // indicador (promt)
                double montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
                System.out.printf("%nsumando %.2f al saldo de cuenta1%n%n",montoDeposito);
                cuenta1.depositar(montoDeposito); // suma al saldo de cuenta1
                // muestra los saldos
                cuenta1.mostrarCuenta();
                cuenta2.mostrarCuenta();
                System.out.print("Escriba el monto a depositar para cuenta2: "); // indicador (promt)
                montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
                System.out.printf("%nsumando %.2f al saldo de cuenta2%n%n",montoDeposito);
                cuenta2.depositar(montoDeposito); // suma al saldo de cuenta2
                // muestra los saldos
                cuenta1.mostrarCuenta();
                cuenta2.mostrarCuenta();
                //pide dinero para retirar
                System.out.println("¿Cuanto dinero quieres retirar de cuenta1?");
                double montoRetiro = entrada.nextDouble(); // obtiene entrada del usuario
                cuenta1.retirar(montoRetiro);
                cuenta1.mostrarCuenta();
                System.out.println("¿Cuanto dinero quieres retirar de cuenta2?");
                montoRetiro = entrada.nextDouble(); // obtiene entrada del usuario
                cuenta2.retirar(montoRetiro);
                cuenta2.mostrarCuenta();
        } // fin de main
}  

