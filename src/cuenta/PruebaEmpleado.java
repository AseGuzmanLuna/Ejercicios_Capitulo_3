package cuenta;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class PruebaEmpleado {
    public static void main(String[] args)
    {
        Empleado empleado1 = new Empleado("Aseret", "Guzmán", 5000.00);
        Empleado empleado2 = new Empleado("Samuel", "Guzmán", 10000.00);
        System.out.printf("Empleado 1: %s %s%nSalario Mesual: %.2f%n", 
                empleado1.obtenerNombre(), empleado1.obtenerApellido(), empleado1.obtenerSalario());
        System.out.printf("Salario anual: %.2f%n", empleado1.obtenerSalario()*12);
        System.out.printf("Empleado 2: %s %s%nSalario Mesual: %.2f%n", 
                empleado2.obtenerNombre(), empleado2.obtenerApellido(), empleado2.obtenerSalario());
        System.out.printf("Salario anual: %.2f%n", empleado2.obtenerSalario()*12);
        System.out.println("Se hace un aumento del %10 a los salarios");
        System.out.printf("Salario anual del empleado1: %.2f%n", empleado1.obtenerSalario()*1.1*12);
        System.out.printf("Salario anual del empleado2: %.2f%n", empleado2.obtenerSalario()*1.1*12);
    }
            Empleado empleado1 = new Empleado("Aseret", "Guzmán", 5000.00);

}
