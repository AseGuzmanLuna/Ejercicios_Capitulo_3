package cuenta;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Empleado {
    private String nombre;
    private String apellido;
    private double salario;
    //constructor que recibe 3 parámetros
    Empleado(String n, String a, double s){
        this.nombre = n;
        this.apellido = a;
        if(s>0)
            this.salario = s;
        else 
            this.salario = 0.0;
    }
        // método que obtiene el nombre
        public String obtenerNombre()
        {
            return nombre;
        }
        // método que establece el nombre
        public void establecerNombre(String n)
        {
            this.nombre = n;
        }
        // método que obtiene el apellido
        public String obtenerApellido()
        {
            return apellido;
        }
        // método que establece el apellido
        public void establecerApellido(String a)
        {
            this.apellido = a;
        }
        //método que obtiene el salario
        public double obtenerSalario()
        {
            return salario;
        }
        // método que establece el salario
        public void establecerSalario(double s)
        {
            if(s>0)
                this.salario = s;
            else 
                this.salario = 0.00;
        }
        
}
